#!/bin/bash
while ! exec 6<>/dev/tcp/config-service/8888; do
    echo "Trying to connect to config-service..."
    sleep 10
done

java -agentlib:jdwp=transport=dt_socket,address=4001,server=y,suspend=n -Xmx200m -jar /app/gateway-service.jar
