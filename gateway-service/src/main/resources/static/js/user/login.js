function loginUser() {

    var username = $("#login").val();
    var password = $("#password").val();

    if (requestOauthToken(username, password)) {
        window.location ='/manager/station.html';
    } else {
        showErrorWindow("Incorrect login or password");
    }
}

$(document).ready(function () {
        hideModalWindow("errorAlertDiv");
    }

);
