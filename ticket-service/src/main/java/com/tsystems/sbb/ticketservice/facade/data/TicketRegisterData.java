package com.tsystems.sbb.ticketservice.facade.data;


public class TicketRegisterData {
    private String trainNumber;
    private String station;

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
}
