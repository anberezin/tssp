package com.tsystems.sbb.ticketservice.api;

import com.tsystems.sbb.ticketservice.facade.TicketFacade;
import com.tsystems.sbb.ticketservice.facade.data.TicketData;
import com.tsystems.sbb.ticketservice.facade.data.TicketResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class TicketController extends BaseController {
    private static final Logger LOG = LoggerFactory.getLogger(TicketController.class);

    @Autowired
    private TicketFacade ticketFacade;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public TicketResponseData createTicket(@RequestBody @Valid TicketData ticket) {
        LOG.debug("TicketController: create ticket for passenger {} request accepted", ticket.getPassenger());
        return ticketFacade.createTicket(ticket);
    }
}
