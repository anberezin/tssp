package com.tsystems.sbb.ticketservice.exception;

public class FutureTimeException extends RuntimeException {
    public FutureTimeException() {
        super("Date can't be in future");
    }
}
