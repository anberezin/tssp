package com.tsystems.sbb.ticketservice.api;


import com.tsystems.sbb.ticketservice.facade.data.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.text.MessageFormat;

public class BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(BaseController.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    private ResponseData handleCommonException(Exception ex) {
        return buildErrorResponse(ex);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private ResponseData handleValidationException(MethodArgumentNotValidException argumentNotValidException) {
        return buildErrorResponse(argumentNotValidException);
    }

    private ResponseData buildErrorResponse(Exception ex) {
        LOG.error(ex.getMessage(), ex);
        ResponseData responseData = new ResponseData();
        responseData.setSuccess(false);
        responseData.setMessage(ex.getMessage());
        return responseData;
    }

    private ResponseData buildErrorResponse(MethodArgumentNotValidException ex) {
        LOG.error(ex.getMessage(), ex);
        ResponseData responseData = new ResponseData();
        responseData.setSuccess(false);
        responseData.setMessage(MessageFormat.format("Entity '{0}' is not valid", ex.getBindingResult().getObjectName()));
        return responseData;
    }
}
