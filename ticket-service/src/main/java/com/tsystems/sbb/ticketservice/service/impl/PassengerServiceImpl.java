package com.tsystems.sbb.ticketservice.service.impl;

import com.tsystems.sbb.ticketservice.entity.Passenger;
import com.tsystems.sbb.ticketservice.reposiory.PassengerRepository;
import com.tsystems.sbb.ticketservice.service.PassengerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PassengerServiceImpl implements PassengerService {

    private static final Logger LOG = LoggerFactory.getLogger(PassengerServiceImpl.class);

    @Autowired
    private PassengerRepository passengerRepository;

    @Override
    public Passenger refresh(Passenger passenger) {
        LOG.debug("PassengerService: searching {} in repository", passenger);
        return passengerRepository.findByNameAndSurnameAndBirthday(passenger.getName(),
                passenger.getSurname(), passenger.getBirthday());
    }
}

