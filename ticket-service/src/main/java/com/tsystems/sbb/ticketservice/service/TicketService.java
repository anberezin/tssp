package com.tsystems.sbb.ticketservice.service;

import com.tsystems.sbb.ticketservice.entity.Ticket;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TicketService {

    /**
     * Checks for given ticket if they not existing then saves it
     * @param ticket - ticket to check and save
     * @return ticket
     */
    Ticket createTicket(Ticket ticket);

    /**
     *  Gets all sold tickets for the given train
     * @param trainNumber - train number to search tickets for this trains
     * @return tickets list for the given trains
     */
    List<Ticket> getTicketForTrainNumber(String trainNumber);

    /**
     * Counts number of sold tickets for the given train
     * @param trainNumber - train number to search sold tickets for this train
     * @return
     */
    int getCountSoldTicketsForTrain(String trainNumber);
}
