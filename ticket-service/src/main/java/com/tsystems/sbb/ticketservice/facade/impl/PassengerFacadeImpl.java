package com.tsystems.sbb.ticketservice.facade.impl;

import com.tsystems.sbb.ticketservice.entity.Passenger;
import com.tsystems.sbb.ticketservice.entity.Ticket;
import com.tsystems.sbb.ticketservice.facade.PassengerFacade;
import com.tsystems.sbb.ticketservice.facade.data.PassengerData;
import com.tsystems.sbb.ticketservice.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PassengerFacadeImpl implements PassengerFacade {

    @Autowired
    private TicketService ticketService;
    @Autowired
    private Converter<Passenger, PassengerData> passengerConverter;

    @Override
    public List<PassengerData> getPassengersForTrain(String trainNumber) {
        return ticketService.getTicketForTrainNumber(trainNumber).stream()
                .map(Ticket::getPassenger)
                .map(passengerConverter::convert)
                .collect(Collectors.toList());
    }
}
