package com.tsystems.sbb.ticketservice.facade;

import com.tsystems.sbb.ticketservice.facade.data.TicketData;
import com.tsystems.sbb.ticketservice.facade.data.TicketResponseData;

public interface TicketFacade {
    /**
     * Creates a new ticket and send request for train-service for registration
     * @param ticket - ticket to check and save
     * @return ticket
     *
     * @throws IllegalArgumentException if ticket registration fails
     */
    TicketResponseData createTicket(TicketData ticket);
}
