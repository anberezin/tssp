package com.tsystems.sbb.ticketservice.facade.impl;

import com.tsystems.sbb.ticketservice.client.TrainServiceClient;
import com.tsystems.sbb.ticketservice.client.data.TrainClientResponse;
import com.tsystems.sbb.ticketservice.entity.Ticket;
import com.tsystems.sbb.ticketservice.exception.TicketSoldOutException;
import com.tsystems.sbb.ticketservice.facade.TicketFacade;
import com.tsystems.sbb.ticketservice.facade.data.TicketData;
import com.tsystems.sbb.ticketservice.facade.data.TicketRegisterData;
import com.tsystems.sbb.ticketservice.facade.data.TicketResponseData;
import com.tsystems.sbb.ticketservice.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TicketFacadeImpl implements TicketFacade {

    @Autowired
    private TicketService ticketService;
    @Autowired
    private TrainServiceClient trainServiceClient;
    @Autowired
    private Converter<TicketData, Ticket> ticketDataConverter;
    @Autowired
    private Converter<Ticket, TicketResponseData> ticketResponseConverter;
    @Autowired
    private Converter<TicketData, TicketRegisterData> ticketRegisterConverter;

    @Override
    @Transactional
    public TicketResponseData createTicket(TicketData ticket) {
        String trainNumber = ticket.getTrainNumber();
        TrainClientResponse response = trainServiceClient.checkAvailableTicketForTrain(ticketRegisterConverter.convert(ticket));
        if(!response.isSuccess()) {
            throw new IllegalArgumentException(response.getMessage());
        }
        if(trainServiceClient.getPlacesForTrain(trainNumber) <= ticketService.getCountSoldTicketsForTrain(trainNumber))
        {
            throw new TicketSoldOutException();
        }
        Ticket savedTicket = ticketService.createTicket(ticketDataConverter.convert(ticket));
        return ticketResponseConverter.convert(savedTicket);
    }
}
