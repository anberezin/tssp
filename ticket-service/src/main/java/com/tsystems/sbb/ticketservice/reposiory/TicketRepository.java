package com.tsystems.sbb.ticketservice.reposiory;

import com.tsystems.sbb.ticketservice.entity.Passenger;
import com.tsystems.sbb.ticketservice.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, String> {
    List<Ticket> findByTrainNumberAndPassenger(String number, Passenger passenger);
    List<Ticket> findByTrainNumber(String trainNumber);
    boolean existsByTrainNumberAndPassenger(String number, Passenger passenger);
    int countAllByTrainNumber(String trainNumber);
}
