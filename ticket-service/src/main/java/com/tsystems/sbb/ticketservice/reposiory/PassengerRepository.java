package com.tsystems.sbb.ticketservice.reposiory;

import com.tsystems.sbb.ticketservice.entity.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger, String> {
    Passenger findByNameAndSurnameAndBirthday(String name, String surname, LocalDate birthday);
}
