package com.tsystems.sbb.ticketservice.facade.converter;

import com.tsystems.sbb.ticketservice.entity.Passenger;
import com.tsystems.sbb.ticketservice.entity.Ticket;
import com.tsystems.sbb.ticketservice.facade.data.PassengerData;
import com.tsystems.sbb.ticketservice.facade.data.TicketData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TicketConverter implements Converter<TicketData, Ticket> {

    @Autowired
    private Converter<PassengerData, Passenger> passengerConverter;

    @Override
    public Ticket convert(TicketData ticketData) {
        Ticket ticket = new Ticket();
        ticket.setTrainNumber(ticketData.getTrainNumber());
        ticket.setPassenger(passengerConverter.convert(ticketData.getPassenger()));
        return ticket;
    }
}
