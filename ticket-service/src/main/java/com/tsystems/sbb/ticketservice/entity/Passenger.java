package com.tsystems.sbb.ticketservice.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "passengers",
        uniqueConstraints = @UniqueConstraint(columnNames = {"name", "surname", "birthday"}))
public class Passenger {

    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    private String id;

    private String name;
    private String surname;
    private LocalDate birthday;
    @OneToMany(mappedBy = "passenger")
    private List<Ticket> tickets;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int hashCode() {
        return new HashCodeBuilder()
                .append(name)
                .append(surname)
                .append(birthday)
                .hashCode();
    }

    public boolean equals(Object obj) {

        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Passenger passenger = (Passenger) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(name, passenger.name)
                .append(surname, passenger.surname)
                .append(birthday, passenger.birthday)
                .isEquals();
    }
}
