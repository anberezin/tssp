package com.tsystems.sbb.ticketservice.reposiory

import com.tsystems.sbb.ticketservice.entity.Passenger
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import spock.lang.Specification

import java.time.LocalDate

@SpringBootTest
class PassengerRepositorySpec extends Specification {
    @Autowired
    PassengerRepository passengerRepository

    def setup() {
        passengerRepository.deleteAll()
    }

    @Test
    "when passenger is created - then id is auto generated"() {
        given:
            def passenger = new Passenger(name: "Name", surname: "Surname", birthday: LocalDate.now())
        when:
            def savedPassenger = passengerRepository.save(passenger)
        then:
            savedPassenger.id
    }

    @Test
    "when passenger exists and try to found it - then passenger is founded"() {
        given:
        def passenger = new Passenger(name: "Name", surname: "Surname", birthday: LocalDate.now())
        and:
        passengerRepository.save(passenger)

        when:
        def foundedPassenger = passengerRepository.findByNameAndSurnameAndBirthday("Name", "Surname", LocalDate.now())
        then:
        foundedPassenger

    }

}
