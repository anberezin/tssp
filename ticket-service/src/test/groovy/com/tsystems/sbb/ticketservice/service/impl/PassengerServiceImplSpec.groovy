package com.tsystems.sbb.ticketservice.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.tsystems.sbb.ticketservice.entity.Passenger
import com.tsystems.sbb.ticketservice.reposiory.PassengerRepository
import org.junit.Test
import spock.lang.Specification

import java.time.LocalDate

class PassengerServiceImplSpec extends Specification {

    @Collaborator
    PassengerRepository passengerRepository = Mock()

    @Subject
    PassengerServiceImpl passengerService

    @Test
    "when passenger exists - then passenger id is refreshed"() {
        given:
        def sourcePassenger = new Passenger(name: "Name", surname: "Surname", birthday: LocalDate.now())
        def foundedPassenger = new Passenger(id: "id")
        and:
        passengerRepository.findByNameAndSurnameAndBirthday("Name","Surname", LocalDate.now()) >> foundedPassenger

        when:
        passengerService.refresh(sourcePassenger)

        then:
        passengerService.refresh(sourcePassenger) == foundedPassenger
    }
}
