#!/bin/bash
while ! exec 6<>/dev/tcp/config-service/8888; do
    echo "Trying to connect to config-service..."
    sleep 10
done
while ! exec 6<>/dev/tcp/ticket-mysqldb/3306; do
    echo "Trying to connect to ticket-mysqldb..."
    sleep 10
done
while ! exec 6<>/dev/tcp/train-service/6000; do
    echo "Trying to connect to train-service..."
    sleep 10
done

java -agentlib:jdwp=transport=dt_socket,address=8001,server=y,suspend=n -Xmx200m -jar /app/ticket-service.jar