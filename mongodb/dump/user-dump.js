db.users.insert(
    {
        "_id" : "root",
        "password" : "$2a$10$8lEieXZSPvHL7RwDmXMS6uqcj0gxRktNYojCVCj83GqPgfxRqXNUK",
        "authorities" : [
            {
                "authority" : "ROLE_MANAGER"
            }
        ]
    }
);