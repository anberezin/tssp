#!/bin/bash
while ! exec 6<>/dev/tcp/config-service/8888; do
    echo "Trying to connect to config service..."
    sleep 10
done

java -Xmx200m -jar /app/registry-service.jar
