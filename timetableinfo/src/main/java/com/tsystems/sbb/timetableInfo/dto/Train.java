package com.tsystems.sbb.timetableInfo.dto;

import java.util.Map;

public class Train {
    private int places;
    private String number;
    private String routeId;
    private String id;
    private String status;

    private Map<String, Timetable> stationSchedules;

    public Map<String, Timetable> getStationSchedules() {
        return stationSchedules;
    }

    public void setStationSchedules(Map<String, Timetable> stationSchedules) {
        this.stationSchedules = stationSchedules;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
