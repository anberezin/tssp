package com.tsystems.sbb.timetableInfo.dto;

public class TrainTimetable {
    private String id;
    private String arrivedTime;
    private String departureTime;
    private String status;
    private Integer shift;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getArrivedTime() {
        return arrivedTime;
    }

    public void setArrivedTime(String arrivedTime) {
        this.arrivedTime = arrivedTime;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrainTimetable that = (TrainTimetable) o;

        if (!id.equals(that.id)) return false;
        if (!arrivedTime.equals(that.arrivedTime)) return false;
        if (!departureTime.equals(that.departureTime)) return false;
        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + arrivedTime.hashCode();
        result = 31 * result + departureTime.hashCode();
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    public TrainTimetable(String id, String arrivedTime, String departureTime, String status) {
        this.id = id;
        this.arrivedTime = arrivedTime;
        this.departureTime = departureTime;
        this.status = status;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getShift() {
        return shift;
    }

    public void setShift(Integer shift) {
        this.shift = shift;
    }
}
