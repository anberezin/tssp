package com.tsystems.sbb.timetableInfo.pusher;

import org.omnifaces.cdi.Push;
import org.omnifaces.cdi.PushContext;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ApplicationScoped
public class PushBean implements Serializable {
    @Inject
    @Push(channel = "clock")
    private PushContext push;

    public void clockAction(){
        push.send("Update in database");
    }
}
