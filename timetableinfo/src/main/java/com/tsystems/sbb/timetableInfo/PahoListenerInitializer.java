package com.tsystems.sbb.timetableInfo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsystems.sbb.timetableInfo.dto.UpdateTimetableData;
import com.tsystems.sbb.timetableInfo.producer.Property;
import com.tsystems.sbb.timetableInfo.pusher.PushBean;
import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.text.MessageFormat;


@Singleton
@Startup
@DependsOn("TimetableInfoLoader")
public class PahoListenerInitializer {

    @Inject
    @Property("hivemq.protocol")
    private String hiveProtocol;
    @Inject
    @Property("hivemq.host")
    private String hiveHost;
    @Inject
    @Property("hivemq.port")
    private String hivePort;
    private static final Logger LOGGER = Logger.getLogger(PahoListenerInitializer.class);

    @Inject
    private TimetableInfoLoader timetableInfoLoader;

    @Inject
    private PushBean pushBean;

    @Inject
    private ObjectMapper objectMapper;

    private MqttClient mqttClient;

    @PostConstruct
    public void init() {
        try {
            mqttClient= new MqttClient(hiveProtocol + "://" + hiveHost + ":" + hivePort, MqttClient.generateClientId());
            mqttClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable throwable) {

                }

                @Override
                public void messageArrived(String station, MqttMessage mqttMessage) throws Exception {
                    LOGGER.info(MessageFormat.format("Station: {0}", station));
                    LOGGER.info(MessageFormat.format("Message: {0}", mqttMessage));
                    UpdateTimetableData updateTimetableData = objectMapper.readValue(mqttMessage.getPayload(), UpdateTimetableData.class);
                    timetableInfoLoader.updateStationTimetableStorage(station, updateTimetableData);
                    pushBean.clockAction();
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

                }
            });
            mqttClient.connect();
        } catch (MqttException e) {
            LOGGER.error(e,e);
        }
    }

    public MqttClient getMqttClient(){
        return mqttClient;
    }

    @PreDestroy
    public void destroy(){
        try {
            mqttClient.close();
        } catch (MqttException e) {
            LOGGER.error(e,e);
        }
    }
}
