package com.tsystems.sbb.trainservice.facade.impl;

import com.tsystems.sbb.trainservice.domain.Status;
import com.tsystems.sbb.trainservice.domain.Train;
import com.tsystems.sbb.trainservice.facade.TrainFacade;
import com.tsystems.sbb.trainservice.facade.data.*;
import com.tsystems.sbb.trainservice.service.MessageService;
import com.tsystems.sbb.trainservice.service.TrainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class TrainFacadeImpl implements TrainFacade {

    private static final Logger LOG = LoggerFactory.getLogger(TrainFacadeImpl.class);

    private static final String ARCHIVED = "ARCHIVED";
    private static final String CANCELED = "CANCELED";
    @Autowired
    private TrainService trainService;
    @Autowired
    private Converter<CreateTrainData, Train> createTrainDataConverter;
    @Autowired
    private Converter<Train, TrainData> trainConverter;
    @Autowired
    private MessageService messageService;

    @Override
    public void createTrain(CreateTrainData trainData) {
        Train train = createTrainDataConverter.convert(trainData);
        trainService.createTrain(train);
        notify(train);
    }

    private void notify(Train train) {
        TrainData trainData = trainConverter.convert(train);
        trainData.getStationSchedules().forEach((station, schedule) -> notify(trainData, station, schedule, false));
    }

    private void notify(TrainData trainData, String station, ScheduleData schedule, boolean refresh) {
        UpdateTimetableData updateTimetableData = new UpdateTimetableData();
        updateTimetableData.setArrive(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(schedule.getArrive()));
        updateTimetableData.setDeparture(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(schedule.getDeparture()));
        if (!refresh) {
            updateTimetableData.setStatus(schedule.getStatus());
            LOG.debug("TrainService: archiving status {}", schedule.getStatus());
        } else {
            updateTimetableData.setStatus(CANCELED);
            LOG.debug("TrainService: archiving status for train canceled");
        }
        updateTimetableData.setTrainNumber(trainData.getNumber());
        updateTimetableData.setRefresh(refresh);
        messageService.sendMessage(station, updateTimetableData);
    }

    @Override
    public List<TrainData> searchTrains(SearchTrainData searchTrainData) {
        return trainService.searchTrain(searchTrainData.getDeparture(), searchTrainData.getArrived(),
                searchTrainData.getStartDate(), searchTrainData.getEndDate())
                .stream()
                .filter(this::filterByStatus)
                .filter(train -> byStatusStations(train, searchTrainData))
                .map(trainConverter::convert)
                .collect(Collectors.toList());
    }

    private boolean byStatusStations(Train train, SearchTrainData searchTrainData) {
        if (train.getStationStatus() != null) {
            Status arrivedStatus = train.getStationStatus().get(searchTrainData.getArrived());
            Status departureStatus = train.getStationStatus().get(searchTrainData.getDeparture());
            if(arrivedStatus != null && Objects.equals(arrivedStatus.getId(), "CANCELED"))
                return false;
            if(departureStatus != null && Objects.equals(departureStatus.getId(), "CANCELED"))
                return false;
        }
        return true;
    }

    @Override
    public List<TrainData> getTrains(String station, boolean filter) {
        Stream<Train> trains = getTrains(station);
        if(filter)
            trains = trains.filter(this::filterByStatus);
        return trains.map(trainConverter::convert).collect(Collectors.toList());
    }

    private boolean filterByStatus(Train train) {
        return !Objects.equals(train.getStatus(), ARCHIVED);
    }

    private Stream<Train> getTrains(String station) {
        return trainService.getTrains(station).stream();
    }

    @Override
    public Integer getPlaceForTrain(String trainNumber) {
        Train train = trainService.getTrainByNumber(trainNumber);
        return train == null ? -1 : train.getPlaces();
    }

    @Override
    public void updateStatus(String trainNumber, String station, String status, Integer delayTime) {
        Train train = trainService.getTrainByNumber(trainNumber);
        if(train.getStationStatus() == null) {
            train.setStationStatus(new HashMap<>());
        }
        Status statusEntity = new Status();
        statusEntity.setId(status);
        statusEntity.setDelayed(delayTime);
        train.getStationStatus().put(station, statusEntity);
        trainService.update(train);
        UpdateTimetableData updateTimetableData = new UpdateTimetableData();
        updateTimetableData.setStatus(status);
        updateTimetableData.setTrainNumber(trainNumber);
        updateTimetableData.setDelayTime(delayTime);
        messageService.sendMessage(station, updateTimetableData);
    }

    @Override
    public void archiveTrain(String id) {
        Train train = trainService.getTrainById(id);
        archiveAndSave(train);
    }

    @Override
    public void archiveTrainByRoute(String routeId) {
        List<Train> trainsByRoutes = trainService.getTrainsByRoutes(Collections.singletonList(routeId));
        trainsByRoutes.forEach(this::archiveAndSave);
    }

    private void archiveAndSave(Train train) {
        LOG.debug("TrainFacade: train ready for archiving {}", train);
        TrainData trainData = trainConverter.convert(train);
        trainData.getStationSchedules().forEach((station, schedule) -> notify(trainData, station, schedule, true));
        LOG.debug("TrainService: archiving {}", train);
        train.setStatus(ARCHIVED);
        trainService.update(train);
    }


}
