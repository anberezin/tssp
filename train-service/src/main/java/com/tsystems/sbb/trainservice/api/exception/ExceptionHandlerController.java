package com.tsystems.sbb.trainservice.api.exception;

import com.tsystems.sbb.trainservice.facade.data.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ControllerAdvice
public class ExceptionHandlerController {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlerController.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus
    public ResponseData handleException(Exception e) {
        LOG.error("Error was occurred", e);
        ResponseData response = new ResponseData();
        response.setSuccess(false);
        response.setMessage(e.getMessage());
        return response;
    }
}
