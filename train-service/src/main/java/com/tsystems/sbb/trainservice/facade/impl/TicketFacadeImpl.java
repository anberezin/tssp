package com.tsystems.sbb.trainservice.facade.impl;

import com.tsystems.sbb.trainservice.domain.Train;
import com.tsystems.sbb.trainservice.facade.TicketFacade;
import com.tsystems.sbb.trainservice.facade.data.TicketRegisterData;
import com.tsystems.sbb.trainservice.facade.data.ResponseData;
import com.tsystems.sbb.trainservice.service.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TicketFacadeImpl implements TicketFacade {

    @Autowired
    private TrainService trainService;

    @Override
    public ResponseData checkAvailableTicketForTrain(TicketRegisterData ticketRegisterData) {
        Train train = trainService.getTrainByNumber(ticketRegisterData.getTrainNumber());
        trainService.checkAvailableTrainByStation(train, ticketRegisterData.getStation());
        return new ResponseData();
    }
}
