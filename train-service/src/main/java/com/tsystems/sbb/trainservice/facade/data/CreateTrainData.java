package com.tsystems.sbb.trainservice.facade.data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;


public class CreateTrainData {

    @NotNull
    private String number;
    @NotNull
    private String routeId;
    @Min(1)
    private int places;

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
