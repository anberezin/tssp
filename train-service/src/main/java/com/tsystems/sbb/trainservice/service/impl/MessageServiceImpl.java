package com.tsystems.sbb.trainservice.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsystems.sbb.trainservice.service.MessageService;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {

    private static final Logger LOG = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Autowired
    private MqttClient mqttClient;
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void sendMessage(String topic, Object message) {
        try {
            byte[] msg = objectMapper.writeValueAsBytes(message);
            MqttMessage mqttMessage = new MqttMessage(msg);
            mqttClient.publish(topic, mqttMessage);
        } catch (MqttException | JsonProcessingException e) {
            LOG.error(e.getMessage(), e);
        }

    }
}
