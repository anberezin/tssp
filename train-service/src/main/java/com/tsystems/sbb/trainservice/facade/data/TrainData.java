package com.tsystems.sbb.trainservice.facade.data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Map;

public class TrainData {
    private String id;
    @NotNull
    @Pattern(regexp = ".*[\\S]+.*")
    private String number;
    private String routeId;
    private String status;
    @Min(1)
    private int places;
    private Map<String, ScheduleData> stationSchedules;

    public Map<String, ScheduleData> getStationSchedules() {
        return stationSchedules;
    }

    public void setStationSchedules(Map<String, ScheduleData> stationSchedules) {
        this.stationSchedules = stationSchedules;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
