package com.tsystems.sbb.trainservice.service;

import com.tsystems.sbb.trainservice.domain.Train;
import com.tsystems.sbb.trainservice.exception.TicketSoldOutException;
import com.tsystems.sbb.trainservice.exception.TrainIsOffException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface TrainService {

    /**
     * Creates a new train and saves it
     *
     * @param train - trains that need to be saved
     * @return trains
     */
    Train createTrain(Train train);

    /**Gets trains by number
     *
     * @param trainNumber - train number
     * @return train
     */
    Train getTrainByNumber(String trainNumber);

    /**
     * Checks available trains for given station, that have  at least 10 minutes before the departure
     * @param train - train
     * @param station - station
     *
     * @throws TrainIsOffException if there are less than 10 minutes before the departure
     */
    void checkAvailableTrainByStation(Train train, String station);

    /**
     * Search train for given departure and arrive stations for the given date
     *
     * @param departureStation - departure station
     * @param arrivedStation - arrived station
     * @param startDate - departure date
     * @return list of available trains
     */
    List<Train> searchTrain(String departureStation, String arrivedStation, LocalDateTime startDate, LocalDateTime endDate);

    /**
     * Gets all trains for the given station
     * @param station - station
     * @return list of available trains
     */
    List<Train> getTrains(String station);

    /**
     * Updates train
     * @param train - train
     */
    void update(Train train);

    /**
     * Gets train by id
     * @param id - train id
     * @return train
     */
    Train getTrainById(String id);

    /**
     * Gets all trains by route
     * @param routeIds - route id
     * @return - trains
     */
    List<Train> getTrainsByRoutes(List<String> routeIds);
}
