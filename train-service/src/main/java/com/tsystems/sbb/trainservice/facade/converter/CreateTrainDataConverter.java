package com.tsystems.sbb.trainservice.facade.converter;

import com.tsystems.sbb.trainservice.domain.Train;
import com.tsystems.sbb.trainservice.facade.data.CreateTrainData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.stream.Collectors;

@Component
public class CreateTrainDataConverter implements Converter<CreateTrainData,Train>{
    @Override
    public Train convert(CreateTrainData source) {
        Train train = new Train();
        train.setNumber(source.getNumber());
        train.setPlaces(source.getPlaces());
        train.setRouteId(source.getRouteId());
        return train;
    }
}
