package com.tsystems.sbb.trainservice.exception;

public class TicketSoldOutException extends RuntimeException {
    public TicketSoldOutException() {
        super("All tickets are sold");
    }
}
