package com.tsystems.sbb.trainservice.service;


public interface MessageService {

    void sendMessage(String topic, Object message);
}
