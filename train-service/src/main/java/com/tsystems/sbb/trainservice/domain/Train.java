package com.tsystems.sbb.trainservice.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Document(collection = "trains")
public class Train implements Serializable {
    @Id
    private String id;

    @Indexed(unique = true)
    private String number;

    private int places;

    private String routeId;

    private String status;

    private Map<String, Status> stationStatus;

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Status> getStationStatus() {
        return stationStatus;
    }

    public void setStationStatus(Map<String, Status> stationStatus) {
        this.stationStatus = stationStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
