package com.tsystems.sbb.trainservice.facade;

import com.tsystems.sbb.trainservice.facade.data.TicketRegisterData;
import com.tsystems.sbb.trainservice.facade.data.ResponseData;

public interface TicketFacade {

    /**
     * Checks available tickets for the train
     *
     * @param ticketRegisterData - ticket register data
     * @return response
     */
    ResponseData checkAvailableTicketForTrain(TicketRegisterData ticketRegisterData);
}
