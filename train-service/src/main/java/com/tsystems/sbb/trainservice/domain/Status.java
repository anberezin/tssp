package com.tsystems.sbb.trainservice.domain;

public class Status {
    private String id;
    private Integer delayed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getDelayed() {
        return delayed;
    }

    public void setDelayed(Integer delayed) {
        this.delayed = delayed;
    }
}
