package com.tsystems.sbb.trainservice.facade.converter;

import com.tsystems.sbb.trainservice.client.RoutesClient;
import com.tsystems.sbb.trainservice.domain.Status;
import com.tsystems.sbb.trainservice.domain.Train;
import com.tsystems.sbb.trainservice.facade.data.RouteData;
import com.tsystems.sbb.trainservice.facade.data.ScheduleData;
import com.tsystems.sbb.trainservice.facade.data.TrainData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Component
public class TrainConverter implements Converter<Train, TrainData> {

    @Autowired
    private RoutesClient routesClient;

    @Override
    public TrainData convert(Train train) {
        TrainData trainData = new TrainData();
        trainData.setId(train.getId());
        trainData.setNumber(train.getNumber());
        trainData.setPlaces(train.getPlaces());
        trainData.setRouteId(train.getRouteId());
        RouteData route = routesClient.getRoute(train.getRouteId());
        trainData.setStationSchedules(route.getStationSchedules());
        trainData.getStationSchedules().forEach((k,v) -> {
            v.setStatus(Optional.ofNullable(train.getStationStatus())
                    .map(stationStatus -> stationStatus.get(k))
                    .map(Status::getId).orElse("OK"));
            Optional.ofNullable(train.getStationStatus())
                    .map(stationStatus -> stationStatus.get(k))
                    .map(Status::getDelayed)
                    .ifPresent(delayed -> {
                        ScheduleData scheduleData = trainData.getStationSchedules().get(k);
                        scheduleData.setArrive(scheduleData.getArrive().plusMinutes(delayed));
                        scheduleData.setDeparture(scheduleData.getDeparture().plusMinutes(delayed));
                    });
        });
        trainData.setStatus(train.getStatus());
        return trainData;
    }
}
