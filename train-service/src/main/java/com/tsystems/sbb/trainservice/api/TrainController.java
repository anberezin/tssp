package com.tsystems.sbb.trainservice.api;

import com.tsystems.sbb.trainservice.facade.TrainFacade;
import com.tsystems.sbb.trainservice.facade.data.CreateTrainData;
import com.tsystems.sbb.trainservice.facade.data.ResponseData;
import com.tsystems.sbb.trainservice.facade.data.SearchTrainData;
import com.tsystems.sbb.trainservice.facade.data.TrainData;
import com.tsystems.sbb.trainservice.service.MessageService;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class TrainController {

    private static final Logger LOG = LoggerFactory.getLogger(TrainController.class);

    @Autowired
    private TrainFacade trainFacade;


    @RequestMapping(method = RequestMethod.POST)
    @Secured("ROLE_MANAGER")
    public ResponseData createTrain(@RequestBody @Valid CreateTrainData trainData) {
        LOG.debug("TrainController: train {} for {} places creation request accepted", trainData.getNumber(), trainData.getPlaces());
        trainFacade.createTrain(trainData);
        return new ResponseData();
    }
    @RequestMapping(method = RequestMethod.GET)
    public List<TrainData> getTrains(@RequestParam(value = "q", required = false) String station) {
        LOG.debug("TrainController: get trains request accepted {}", station);
        return trainFacade.getTrains(station, true);
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public List<TrainData> searchTrains(@RequestParam("departure") String departure, @RequestParam("arrived") String arrived,
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam("startDate") LocalDateTime startDate,
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam("endDate") LocalDateTime endDate) {
        SearchTrainData searchTrainData = new SearchTrainData();
        searchTrainData.setArrived(arrived);
        searchTrainData.setDeparture(departure);
        searchTrainData.setEndDate(endDate);
        searchTrainData.setStartDate(startDate);
        LOG.debug("TrainController: search available trains for {} between {} and {}", searchTrainData.getStartDate(), searchTrainData.getDeparture(), searchTrainData.getArrived());
        return trainFacade.searchTrains(searchTrainData);
    }

    @RequestMapping(path = "/status", method = RequestMethod.PUT)
    @Secured("ROLE_MANAGER")
    public ResponseData updateStatus(@RequestParam("trainNumber") String trainNumber,
                                     @RequestParam("station") String station,
                                     @RequestParam("status") String status,
                                     @RequestParam(required = false, value = "delayTime") Integer delayTime) throws MqttException {
        trainFacade.updateStatus(trainNumber, station, status, delayTime);
        return new ResponseData();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MANAGER")
    public ResponseData archiveTrain(@PathVariable("id") String id) {
        trainFacade.archiveTrain(id);
        return new ResponseData();
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    @Secured("ROLE_MANAGER")
    public List<TrainData> getAllTrains() {
        return trainFacade.getTrains(null, false);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void archiveByRoutes(@RequestBody String routeId) {
        trainFacade.archiveTrainByRoute(routeId);
    }


}
