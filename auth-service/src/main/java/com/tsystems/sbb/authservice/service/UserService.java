package com.tsystems.sbb.authservice.service;

import com.tsystems.sbb.authservice.domain.User;


public interface UserService {
    void create(User user);
}
