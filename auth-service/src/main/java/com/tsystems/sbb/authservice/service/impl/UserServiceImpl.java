package com.tsystems.sbb.authservice.service.impl;

import com.tsystems.sbb.authservice.domain.User;
import com.tsystems.sbb.authservice.repository.UserRepository;
import com.tsystems.sbb.authservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void create(User user) {
        userRepository.save(user);
    }
}
