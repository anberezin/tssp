#!/bin/bash
while ! exec 6<>/dev/tcp/config-service/8888; do
    echo "Trying to connect to config-service..."
    sleep 10
done
while ! exec 6<>/dev/tcp/station-mysqldb/3306; do
    echo "Trying to connect to station-mysqldb..."
    sleep 10
done

java -agentlib:jdwp=transport=dt_socket,address=7001,server=y,suspend=n -Xmx200m -jar /app/station-service.jar