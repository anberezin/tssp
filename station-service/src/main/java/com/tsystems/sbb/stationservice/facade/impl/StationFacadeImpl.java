package com.tsystems.sbb.stationservice.facade.impl;

import com.tsystems.sbb.stationservice.entity.Station;
import com.tsystems.sbb.stationservice.facade.RouteFacade;
import com.tsystems.sbb.stationservice.facade.StationFacade;
import com.tsystems.sbb.stationservice.facade.data.StationData;
import com.tsystems.sbb.stationservice.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class StationFacadeImpl implements StationFacade {

    private static final String ARCHIVED = "ARCHIVED";
    @Autowired
    private StationService stationService;
    @Autowired
    private Converter<StationData, Station> stationDataConverter;
    @Autowired
    private Converter<Station, StationData> stationConverter;
    @Autowired
    private RouteFacade routeFacade;

    @Override
    public void createStation(StationData stationData) {
        Station station = stationDataConverter.convert(stationData);
        stationService.createStation(station);
    }

    @Override
    public List<StationData> getStations(String station, boolean filter) {
        Stream<Station> stations = getStations(station);
        if(filter)
            stations = stations.filter(this::filterByStatus);
        return stations.map(stationConverter::convert).collect(Collectors.toList());
    }

    @Override
    public void archiveStation(String id) {
        Station station = stationService.getStationById(id);
        station.setStatus(ARCHIVED);
        stationService.updateStation(station);
        routeFacade.archiveRouteByStation(station.getName());
    }

    private Stream<Station> getStations(String station) {
        return stationService.getStations(station).stream();
    }

    private boolean filterByStatus(Station station) {
        return !Objects.equals(station.getStatus(), ARCHIVED);
    }
}
