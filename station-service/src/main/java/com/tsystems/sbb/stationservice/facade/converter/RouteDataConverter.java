package com.tsystems.sbb.stationservice.facade.converter;

import com.tsystems.sbb.stationservice.entity.Route;
import com.tsystems.sbb.stationservice.entity.RouteEntry;
import com.tsystems.sbb.stationservice.entity.Schedule;
import com.tsystems.sbb.stationservice.facade.data.RouteData;
import com.tsystems.sbb.stationservice.facade.data.ScheduleData;
import com.tsystems.sbb.stationservice.service.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class RouteDataConverter implements Converter<RouteData, Route>{

    @Autowired
    private StationService stationService;
    @Autowired
    private Converter<ScheduleData, Schedule> scheduleDataConverter;

    @Override
    public Route convert(RouteData source) {
        Route route = new Route();
        route.setRouteEntries(source.getStationSchedules().entrySet().stream().map(this::toRouteEntry)
                .collect(Collectors.toCollection(LinkedList::new)));
        route.setStatus(source.getStatus());
        return route;
    }

    private RouteEntry toRouteEntry(Map.Entry<String, ScheduleData> entryStationSchedule) {
        RouteEntry routeEntry = new RouteEntry();
        routeEntry.setStation(stationService.getStationByName(entryStationSchedule.getKey()));
        routeEntry.setSchedule(scheduleDataConverter.convert(entryStationSchedule.getValue()));
        return routeEntry;
    }
}
