package com.tsystems.sbb.stationservice.api;


import com.tsystems.sbb.stationservice.facade.RouteFacade;
import com.tsystems.sbb.stationservice.facade.data.ResponseData;
import com.tsystems.sbb.stationservice.facade.data.RouteData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(path = "/routes")
public class RoutesController extends BaseController{

    private static final Logger LOG = LoggerFactory.getLogger(RoutesController.class);

    @Autowired
    private RouteFacade routeFacade;

    @RequestMapping(method = RequestMethod.POST)
    @Secured("ROLE_MANAGER")
    public ResponseData createRoute(@RequestBody @Valid RouteData routeData) {
        LOG.debug("RoutesController: {} create request accepted", routeData.getId());
        routeFacade.createRoute(routeData);
        return new ResponseData();
    }



    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public RouteData getRoute(@PathVariable("id") String id) {
        LOG.debug("RoutesController: find route by id {} request accepted", id);
        return routeFacade.getRouteById(id);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MANAGER")
    public ResponseData archiveRoute(@PathVariable("id") String id) {
        routeFacade.archiveRoute(id);
        return new ResponseData();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<RouteData> searchByStations(@RequestParam(value = "departureStation", required = false) String departureStation,
                                            @RequestParam(value = "arriveStation", required = false) String arriveStation,
                                            @RequestParam(value = "station", required = false) String station) {
        LOG.debug("RoutesController: search request accepted");
        if(station != null)  {
            LOG.debug("RoutesController: search routes by station {}", station);
            return routeFacade.getRoutesByStation(station);
        }
        else {
            LOG.debug("RoutesController: search routes from {} to {}", departureStation, arriveStation);
            return (departureStation == null && arriveStation == null) ? routeFacade.getRoutes(true) :
                    ((departureStation == null || arriveStation == null) ? Collections.emptyList() :
                            routeFacade.getRoutesByStations(departureStation, arriveStation));
        }
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    @Secured("ROLE_MANAGER")
    public List<RouteData> getAllRoutes() {
        return routeFacade.getRoutes(false);
    }

}
