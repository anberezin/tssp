package com.tsystems.sbb.stationservice.facade.data;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class StationData {

    private String id;

    @NotNull
    @Pattern(regexp = ".*[\\S]+.*")
    private String name;

    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
