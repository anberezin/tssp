package com.tsystems.sbb.stationservice.facade.converter;

import com.tsystems.sbb.stationservice.entity.Schedule;
import com.tsystems.sbb.stationservice.facade.data.ScheduleData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ScheduleDataConverter implements Converter<ScheduleData, Schedule> {
    @Override
    public Schedule convert(ScheduleData source) {
        Schedule schedule = new Schedule();
        schedule.setDeparture(source.getDeparture());
        schedule.setArrive(source.getArrive());
        return schedule;
    }
}
