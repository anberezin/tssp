package com.tsystems.sbb.stationservice.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("train-service")
public interface TrainServiceClient {

    @RequestMapping(method = RequestMethod.DELETE, value = "/train-service/")
    void archiveByRouteId(@RequestBody String routeId);
}
