package com.tsystems.sbb.stationservice.facade.data;

public class ResponseData {
    private boolean success = true;
    private String message;

    public ResponseData() {}

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
