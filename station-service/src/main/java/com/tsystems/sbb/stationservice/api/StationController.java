package com.tsystems.sbb.stationservice.api;

import com.tsystems.sbb.stationservice.facade.StationFacade;
import com.tsystems.sbb.stationservice.facade.data.ResponseData;
import com.tsystems.sbb.stationservice.facade.data.StationData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
import java.util.List;

@RestController
public class StationController extends BaseController {

    private static final Logger LOG = LoggerFactory.getLogger(StationController.class);

    @Autowired
    private StationFacade stationFacade;

    @RequestMapping(method = RequestMethod.POST)
    @Secured("ROLE_MANAGER")
    public ResponseData createStation(@RequestBody @Valid StationData stationData) {
        LOG.debug("StationController: {} create request accepted", stationData.getName());
        stationFacade.createStation(stationData);
        return new ResponseData();
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<StationData> getStations(@RequestParam(value = "q", required = false) String query) {
        if (query == null) {
            LOG.debug("StationController: get all request accepted");
        } else {
            LOG.debug("StationController: get {} request accepted", query);
        }

        return stationFacade.getStations(query, true);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    @Secured("ROLE_MANAGER")
    public ResponseData archiveStation(@PathVariable("id") String id) {
        stationFacade.archiveStation(id);
        return new ResponseData();
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    @Secured("ROLE_MANAGER")
    public List<StationData> getAllStations() {
        return stationFacade.getStations(null, false);
    }



}
