package com.tsystems.sbb.stationservice.service.impl;

import com.tsystems.sbb.stationservice.api.RoutesController;
import com.tsystems.sbb.stationservice.entity.Route;
import com.tsystems.sbb.stationservice.entity.RouteEntry;
import com.tsystems.sbb.stationservice.entity.Station;
import com.tsystems.sbb.stationservice.repository.RouteRepository;
import com.tsystems.sbb.stationservice.repository.StationRepository;
import com.tsystems.sbb.stationservice.service.RouteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RouteServiceImpl implements RouteService {

    private static final Logger LOG = LoggerFactory.getLogger(RouteServiceImpl.class);

    @Autowired
    private RouteRepository routeRepository;
    @Autowired
    private StationRepository stationRepository;

    @Override
    public Route find(String id) {
        return routeRepository.findOne(id);
    }

    @Override
    public List<Route> getRoutesByStations(String departureStation, String arriveStation) {
        LOG.debug("RouteService: find {} station in repository", departureStation);
        Station departureStationEntity = stationRepository.findByName(departureStation);
        LOG.debug("RouteService: find {} station in repository", arriveStation);
        Station arriveStationEntity = stationRepository.findByName(arriveStation);
        if(departureStationEntity == null || arriveStationEntity == null)
            return Collections.emptyList();
        LOG.debug("RouteService: find routes between {} and {} stations in repository", departureStationEntity.getName(), arriveStationEntity.getName());
        List<Route> foundedRoutes = routeRepository.findByStations(departureStationEntity, arriveStationEntity);
        LOG.debug("RouteService: found {} routes", foundedRoutes.size());
        return foundedRoutes.stream().filter(route -> filterRoute(route, departureStation, arriveStation)).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void createRoute(Route route) {
        routeRepository.save(route);
    }

    @Override
    public List<Route> getRoutesByStation(String station) {
        LOG.debug("RouteService: find {} station in repository", station);
        Station stationEntity = stationRepository.findByName(station);
        LOG.debug("RouteService: foud {} station in repository", stationEntity.getName());
        if(stationEntity == null)
            throw new RuntimeException();
        return routeRepository.findByStation(stationEntity);
    }

    @Override
    public List<Route> getRoutes() {
        return routeRepository.findAll();
    }

    @Override
    public Route getRouteById(String id) {
        return routeRepository.findOne(id);
    }

    @Override
    public void update(Route route) {
        routeRepository.save(route);
    }

    private boolean filterRoute(Route route, String departureStation, String arriveStation) {
        for(RouteEntry routeEntry : route.getRouteEntries()) {
            if(Objects.equals(routeEntry.getStation().getName(), departureStation) || Objects.equals(routeEntry.getStation().getName(), arriveStation)) {
                return Objects.equals(routeEntry.getStation().getName(), departureStation);
            }
        }
        return false;
    }
}
