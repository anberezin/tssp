package com.tsystems.sbb.stationservice.facade.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsystems.sbb.stationservice.validation.constraint.ScheduleConstraint;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteData {
    private String id;
    private String status;

    @NotNull
    @Size(min = 2)
    @ScheduleConstraint
    private Map<String, ScheduleData> stationSchedules;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, ScheduleData> getStationSchedules() {
        return stationSchedules;
    }

    public void setStationSchedules(Map<String, ScheduleData> stationSchedules) {
        this.stationSchedules = stationSchedules;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
