package com.tsystems.sbb.stationservice.facade.converter;

import com.tsystems.sbb.stationservice.entity.Station;
import com.tsystems.sbb.stationservice.facade.data.StationData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StationConverter implements Converter<Station, StationData>{
    @Override
    public StationData convert(Station station) {
        StationData stationData = new StationData();
        stationData.setId(station.getId());
        stationData.setName(station.getName());
        stationData.setStatus(station.getStatus());
        return stationData;
    }
}
